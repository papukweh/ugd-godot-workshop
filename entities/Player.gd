extends KinematicBody2D

export(float) var max_speed = 75.0
var velocity = Vector2(0,0)
var spell = preload("res://objects/Spell.tscn")

const SHOOT_SOUND = preload("res://assets/sfx/sfx_weapon_singleshot2.wav")
const DMG_SOUND = preload("res://assets/sfx/sfx_damage_hit2.wav")
var LIFE = 5

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("idle")

func _physics_process(delta):
	velocity = Vector2(0,0)
	if Input.is_action_pressed("ui_down"):
		velocity += Vector2(0,1)
	if Input.is_action_pressed("ui_up"):
		velocity += Vector2(0,-1)
	if Input.is_action_pressed("ui_right"):
		$Sprite.scale.x = 1
		velocity += Vector2(1,0)
	if Input.is_action_pressed("ui_left"):
		$Sprite.scale.x = -1
		velocity += Vector2(-1,0)
	if Input.is_action_just_pressed("shoot"):
		$SFX.stream = SHOOT_SOUND
		$SFX.play()
		var shoot = spell.instance()
		shoot.shoot(get_global_mouse_position(), self.get_global_position())
		self.get_parent().add_child(shoot)

	if velocity.length() > 0:
		$AnimationPlayer.play("walk")
	else:
		$AnimationPlayer.play("idle")
	
	velocity = move_and_slide(velocity*max_speed)

func _on_Area2D_body_entered(body):
	if body.is_in_group("Enemy"):
		velocity = -(body.position - self.position).normalized() * 2500
		move_and_slide(velocity)
		$SFX.stream = DMG_SOUND
		$SFX.play()
		self.LIFE -= 1
		if self.LIFE <= 0:
			self.queue_free()
